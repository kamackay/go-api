package persist

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/go-api/database"
	"gitlab.com/kamackay/go-api/logging"
	"gitlab.com/kamackay/go-api/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
	"time"
)

func NewService(db *database.DB) *PersistedDataService {
	return &PersistedDataService{
		log:        logging.GetLogger(),
		db:         db,
		semaphores: make(map[string]*sync.Mutex),
	}
}

type PersistedDataService struct {
	log        *logrus.Logger
	collection *mongo.Collection
	db         *database.DB
	semaphores map[string]*sync.Mutex
}

func (s *PersistedDataService) getCollection() *mongo.Collection {
	if s.collection == nil {
		s.collection = s.db.Api.Collection("persistedData", &options.CollectionOptions{})

		// Init important collection info
		// Don't care if there's an error, just means it already exists
		_, _ = s.collection.Indexes().CreateOne(context.Background(), mongo.IndexModel{
			Keys:    bson.D{{Key: "key", Value: 1}},
			Options: options.Index().SetUnique(true),
		})
	}
	return s.collection
}

func (s *PersistedDataService) Save(key string, value string) {
	mutex := utils.LookupOrAdd(s.semaphores, key, makeWG)
	val := value
	go func() {
		mutex.Lock()
		defer mutex.Unlock()
		s.doSave(key, val)
	}()
}

func (s *PersistedDataService) doSave(key string, value string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	collection := s.getCollection()
	_, err := collection.UpdateOne(ctx, bson.M{"key": bson.M{"$eq": key}}, bson.M{"$set": bson.M{"value": value}},
		&options.UpdateOptions{Upsert: utils.Pointer(true)})
	if err != nil {
		s.log.Warnf("Error persisting %s to %s: %+v", value, key, err)
	}
}

func (s *PersistedDataService) Load(key string) (string, error) {
	var result struct {
		Value string `bson:"value"`
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	collection := s.getCollection()
	r := collection.FindOne(ctx, bson.M{"key": bson.M{"$eq": key}})
	err := r.Decode(&result)
	if err != nil {
		return "", err
	}
	return result.Value, nil
}

func makeWG() *sync.Mutex {
	o := sync.Mutex{}
	return &o
}

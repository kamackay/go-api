package news

import (
	"context"
	"gitlab.com/kamackay/go-api/database"
	"gitlab.com/kamackay/go-api/logging"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io/ioutil"
	"net/http"
)

var db = database.New()

func GetCount() (int64, error) {
	var ctx = context.Background()
	var newsCollection = db.Api.Collection("news")
	return newsCollection.CountDocuments(ctx, bson.M{})
}

func GetAllNews() ([]bson.M, error) {
	var log = logging.GetLogger()
	var ctx = context.Background()
	var news []bson.M
	var newsCollection = db.Api.Collection("news")
	cur, err := newsCollection.Find(ctx, bson.M{},
		options.Find().
			SetSort(GetSort()).
			SetLimit(1000))
	if err != nil {
		log.Fatal("Could Not Get News from the collection", err)
		return nil, err
	}
	defer cur.Close(ctx)
	if err = cur.All(ctx, &news); err != nil {
		log.Fatal(err)
	}
	return news, nil
}

func GetSort() bson.M {
	defaultVal := bson.M{
		"priority": -1,
		"time":     -1,
	}
	request, err := http.NewRequest(http.MethodGet, "http://api-service:8080/news/sort", nil)
	if err != nil {
		return defaultVal
	}
	response, err := (&http.Client{}).Do(request)
	if err != nil {
		return defaultVal
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return defaultVal
	}
	var value bson.M
	if bson.Unmarshal(body, &value) != nil {
		return defaultVal
	} else {
		return value
	}
}

func GetNewsAfter(time uint64) ([]bson.M, error) {
	var log = logging.GetLogger()
	var ctx = context.Background()
	var news = make([]bson.M, 0)
	var newsCollection = db.Api.Collection("news")
	cur, err := newsCollection.Find(ctx, bson.M{"time": bson.M{"$gt": time}},
		options.Find().SetLimit(1000))
	if err != nil {
		log.Fatal("Could Not Get News from the collection", err)
		return nil, err
	}
	defer cur.Close(ctx)
	for cur.Next(ctx) {
		var obj bson.M
		err := cur.Decode(&obj)
		if err != nil {
			log.Panic(err)
		} else {
			news = append(news, obj)
		}
	}
	return news, nil
}

type HasId struct {
	ID primitive.ObjectID `bson:"_id, omitempty"`
}

func GetIdsAfter(time uint64) ([]string, error) {
	var log = logging.GetLogger()
	var ctx = context.Background()
	var ids = make([]string, 0)
	var newsCollection = db.Api.Collection("news")
	cur, err := newsCollection.Find(ctx, bson.M{"time": bson.M{"$gt": time}},
		options.Find().SetLimit(1000))
	if err != nil {
		log.Fatal("Could Not Get News from the collection", err)
		return nil, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var hasId HasId
		err := cur.Decode(&hasId)
		if err != nil {
			log.Panic(err)
		} else {
			ids = append(ids, hasId.ID.Hex())
		}
	}

	return ids, nil
}

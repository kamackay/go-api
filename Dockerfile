FROM golang:alpine as builder

WORKDIR /app/
WORKDIR $GOPATH/src/gitlab.com/kamackay/go-api

RUN apk upgrade --update --no-cache && \
        apk add --no-cache \
            git \
            gcc \
            curl \
            linux-headers \
            brotli \
            build-base

ADD ./go.mod ./

RUN go mod download && go mod verify

ADD ./ ./

RUN go build -tags=jsoniter -o server ./*.go && cp ./server /app/

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/server /app/

CMD ["./server"]

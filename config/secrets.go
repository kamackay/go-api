package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func GetCredentialString(key string) (string, error) {
	return ReadJSONFieldFromFile("/api/creds.json", key)
}

// ReadJSONFieldFromFile reads a JSON file from disk and returns the value of the specific field at the given key.
func ReadJSONFieldFromFile(filePath string, key string) (string, error) {
	// Open the JSON file
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// Read the file content
	byteValue, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	// Unmarshal the JSON content into a map
	var result map[string]interface{}
	err = json.Unmarshal(byteValue, &result)
	if err != nil {
		return "", err
	}

	// Get the value for the given key
	value, exists := result[key]
	if !exists {
		return "", fmt.Errorf("key %s not found in JSON file", key)
	}

	return value.(string), nil
}

docker build . --platform=linux/amd64 -t registry.gitlab.com/kamackay/go-api:$1 && \
    docker push registry.gitlab.com/kamackay/go-api:$1 && \
    kubectl --context do-nyc3-keithmackay-cluster -n webpage \
    set image deployment/go-api api=registry.gitlab.com/kamackay/go-api:$1

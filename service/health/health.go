package health

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/go-api/logging"
)

type Verifier interface {
	IsHealthy() bool
	Name() string
}

type Service struct {
	log       *logrus.Logger
	verifiers []Verifier
}

func NewService() *Service {
	return &Service{log: logging.GetLogger()}
}

func (s *Service) RegisterVerifier(verifier Verifier) {
	s.verifiers = append(s.verifiers, verifier)
}

func (s *Service) IsHealthy() bool {
	for _, v := range s.verifiers {
		if !v.IsHealthy() {
			s.log.Warnf("Health check failed for %s", v.Name())
			return false
		}
	}
	return true
}

type verifierInstance struct {
	name string
	f    func() bool
}

func (v *verifierInstance) IsHealthy() bool {
	return v.f()
}

func (v *verifierInstance) Name() string {
	return v.name
}

func NewVerifier(name string, f func() bool) Verifier {
	return &verifierInstance{name: name, f: f}
}

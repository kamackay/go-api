package blocklist

import (
	"context"
	"errors"
	"slices"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/r3labs/sse/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/go-api/config"
	"gitlab.com/kamackay/go-api/database"
	"gitlab.com/kamackay/go-api/logging"
	"gitlab.com/kamackay/go-api/persist"
	"gitlab.com/kamackay/go-api/service/health"
	"gitlab.com/kamackay/go-api/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/sync/errgroup"
)

const (
	NextDNSSilenceTime = 5 * time.Minute
	PersistedLastIdKey = "lastDomainEventId"
)

type Service struct {
	log           *logrus.Logger
	db            *database.DB
	healthService *health.Service
	lastEvent     *time.Time
	collection    *mongo.Collection
	persistedData *persist.PersistedDataService
}

type ServerHitCount struct {
	Server  string              `bson:"server" json:"server"`
	Hits    int64               `bson:"hits" json:"hits"`
	LastHit primitive.Timestamp `bson:"lastHit" json:"lastHit"`
}

func NewService(healthService *health.Service, db *database.DB, persistedDataService *persist.PersistedDataService) *Service {
	return &Service{
		log:           logging.GetLogger(),
		db:            db,
		healthService: healthService,
		lastEvent:     utils.Pointer(time.Now()),
		persistedData: persistedDataService,
	}
}

func (s *Service) InitVerifier() {
	s.healthService.RegisterVerifier(health.NewVerifier("Blocklist", func() bool {
		return true
		//return time.Since(*s.lastEvent) > NextDNSSilenceTime
	}))
}

func (s *Service) getCollection() *mongo.Collection {
	if s.collection == nil {
		s.collection = s.db.Api.Collection("lsrules")
	}
	return s.collection
}

func getUsedFilter() bson.M {
	return bson.M{"$and": []bson.M{{"hits": bson.M{"$gt": 0}}, {"hits": bson.M{"$exists": true}}}}
}

func (s *Service) GetAllSorted() ([]string, error) {
	start := time.Now()
	servers, err := s.GetAll()
	if err != nil {
		return nil, err
	}
	s.log.Infof("Retrieved %d servers in %s", len(servers), time.Since(start))
	times := 0
	slices.SortFunc(servers, func(a, b string) int {
		times++
		splitA := strings.Split(a, ".")
		splitB := strings.Split(b, ".")
		slices.Reverse(splitA)
		slices.Reverse(splitB)
		for i := 0; i < len(splitA) && i < len(splitB); i++ {
			comp := strings.Compare(splitA[i], splitB[i])
			if comp != 0 {
				return comp
			}
		}
		if len(splitA) > len(splitB) {
			return 1
		} else if len(splitA) < len(splitB) {
			return -1
		}
		return 0
	})
	s.log.Infof("Sorted %d servers in %s (%d)", len(servers), time.Since(start), times)
	return servers, nil
}

func (s *Service) GetAll() ([]string, error) {
	return s.getServersFiltered(bson.M{})
}

func (s *Service) getServersFiltered(filter bson.M) ([]string, error) {
	var result []struct {
		Server string `bson:"server"`
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	collection := s.getCollection()
	cursor, err := collection.Find(ctx, filter)
	defer cursor.Close(ctx)
	if err != nil {
		s.log.Error(err)
		return make([]string, 0), err
	}
	if err := cursor.All(ctx, &result); err != nil {
		s.log.Error(err)
		return make([]string, 0), err
	}
	servers := make([]string, len(result))
	for i, r := range result {
		servers[i] = r.Server
	}
	return servers, nil
}

func (s *Service) countServersFiltered(filter bson.M) (int64, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	collection := s.getCollection()
	count, err := collection.CountDocuments(ctx, filter)
	if err != nil {
		s.log.Error(err)
		return 0, err
	}
	return count, nil
}

func (s *Service) getSubUrl() string {
	lastId, err := s.persistedData.Load(PersistedLastIdKey)
	if len(lastId) == 0 || err != nil {
		s.log.Info("Starting Stream Fresh")
		return "https://api.nextdns.io/profiles/22c2ce/logs/stream"
	}
	s.log.Infof("Resuming Stream from ID %s", lastId)
	return "https://api.nextdns.io/profiles/22c2ce/logs/stream?id=" + lastId
}

func (s *Service) ListenForNextDNSLogs() {
	defer func() {
		if r := recover(); r != nil {
			s.log.Error(r)
		}
	}()
	s.log.Info("Listening for new DNS logs")
	key, err := config.GetCredentialString("next-dns-key")
	if err != nil {
		s.log.Error(err)
		return
	}
	client := sse.NewClient(s.getSubUrl(), func(client *sse.Client) {
		client.Headers["X-Api-Key"] = key
	})
	events := make(chan *sse.Event)
	var timer *time.Timer
	restart := func() {
		s.log.Infof("Rescheduled DNS Stream Refresh")
		client.Unsubscribe(events)
		s.ListenForNextDNSLogs()
		timer.Stop()
	}
	go func() {
		err := client.SubscribeChanRaw(events)
		if err != nil {
			s.log.Error(err)
			s.ListenForNextDNSLogs()
		}
		for {
			select {
			case msg := <-events:
				if msg == nil {
					return
				}
				if len(msg.ID) > 0 {
					s.persistedData.Save(PersistedLastIdKey, string(msg.ID))
				} else {
					continue
				}
				s.handleNextDNSLog(msg)
			case <-time.After(NextDNSSilenceTime - time.Minute):
				s.log.Infof("Refreshing stale stream")
				restart()
				return
			}
		}
	}()
	timer = time.AfterFunc(time.Hour, restart)
}

func (s *Service) handleNextDNSLog(msg *sse.Event) {
	s.lastEvent = utils.Pointer(time.Now())
	defer func() {
		recover()
	}()
	rawData := msg.Data
	if len(rawData) == 0 {
		s.log.Debugf("Skipping empty body")
		return
	}
	data, err := utils.MarshalJSONToMap(string(rawData))
	if err != nil {
		s.log.Errorf("Couldn't parse %s, %+v", string(rawData), err)
		return
	}
	domain := data["domain"].(string)
	status := data["status"].(string)
	if status == "default" {
		return
	} else if status == "blocked" {
		//s.log.Infof("Blocked: %s", domain)
		s.handleBlockedServer(domain)
	}
	//fmt.Println(utils.PrettyPrintJSON(data))
}

func (s *Service) handleBlockedServer(domain string) {
	collection := s.getCollection()
	// Define the filter to find the document by ID
	filter := bson.M{"server": domain}

	// Define the update to increment the attribute
	update := bson.M{
		"$inc": bson.M{"hits": 1},
		"$set": bson.M{"lastHit": primitive.Timestamp{T: uint32(time.Now().Unix())}},
	}

	// Options to return the updated document after the update
	opts := options.FindOneAndUpdate().SetReturnDocument(options.After)

	// Perform the update
	var updatedDoc bson.M
	err := collection.FindOneAndUpdate(context.TODO(), filter, update, opts).Decode(&updatedDoc)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			s.log.Infof("Domain %s was not in the database", domain)
			return
		}
		return
	}

	s.log.Infof("Blocked: %s - %d times", domain, updatedDoc["hits"])
}

func (s *Service) GetUnused() ([]string, error) {
	return s.getServersFiltered(bson.M{"$or": []bson.M{{"hits": bson.M{"$eq": 0}}, {"hits": bson.M{"$exists": false}}}})
}

func (s *Service) GetUsed() ([]ServerHitCount, error) {
	var result []ServerHitCount
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	collection := s.getCollection()
	cursor, err := collection.Find(ctx, getUsedFilter(),
		options.Find().SetSort(bson.M{"hits": -1}))
	defer cursor.Close(ctx)
	if err != nil {
		s.log.Error(err)
		return make([]ServerHitCount, 0), err
	}
	if err := cursor.All(ctx, &result); err != nil {
		s.log.Error(err)
		return make([]ServerHitCount, 0), err
	}
	return result, nil
}

func (s *Service) GetRecentlyUsed() ([]string, error) {
	var result []ServerHitCount
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	collection := s.getCollection()
	cursor, err := collection.Find(ctx, getUsedFilter(),
		options.Find().SetSort(bson.M{"hits": -1}))
	defer cursor.Close(ctx)
	if err != nil {
		s.log.Error(err)
		return make([]string, 0), err
	}
	if err := cursor.All(ctx, &result); err != nil {
		s.log.Error(err)
		return make([]string, 0), err
	}
	domains := make([]string, len(result))
	slices.SortStableFunc(result, func(a, b ServerHitCount) int {
		return a.LastHit.Compare(b.LastHit) * -1
	})
	for x, s := range result {
		domains[x] = s.Server
	}
	return domains, nil
}

func (s *Service) GetStats(ctx context.Context) (gin.H, error) {
	start := time.Now()
	group, ctx := errgroup.WithContext(ctx)
	group.SetLimit(4)
	var total int64
	var uploaded int64
	var used int64
	latencies := gin.H{}

	group.Go(lookupData(func() (err error) {
		total, err = s.countServersFiltered(bson.M{})
		return err
	}, func(init time.Time) {
		latencies["total"] = time.Since(init).String()
	}))

	group.Go(lookupData(func() (err error) {
		uploaded, err = s.countServersFiltered(bson.M{"uploadedToNext": bson.M{"$eq": true}})
		return err
	}, func(init time.Time) {
		latencies["uploaded"] = time.Since(init).String()
	}))

	group.Go(lookupData(func() (err error) {
		used, err = s.countServersFiltered(getUsedFilter())
		return err
	}, func(init time.Time) {
		latencies["used"] = time.Since(init).String()
	}))

	if err := group.Wait(); err != nil {
		return nil, err
	}
	latencies["all"] = time.Since(start).String()
	return gin.H{
		"total":              total,
		"count":              uploaded,
		"used":               used,
		"percentageUploaded": (float64(uploaded) * 100) / float64(total),
		"percentageUsed":     (float64(used) * 100) / float64(total),
		"latency":            latencies,
	}, nil
}

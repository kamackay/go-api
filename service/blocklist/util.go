package blocklist

import "time"

func lookupData(main func() error, cleanup func(init time.Time)) func() (err error) {
	return func() error {
		init := time.Now()
		defer func() {
			cleanup(init)
		}()
		return main()
	}
}

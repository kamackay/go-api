package routes

import "github.com/gin-gonic/gin"

type EndpointGroup interface {
	Routes(router *gin.Engine)
	Init()
}

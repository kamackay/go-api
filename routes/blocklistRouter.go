package routes

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/go-api/config"
	"gitlab.com/kamackay/go-api/logging"
	"gitlab.com/kamackay/go-api/service/blocklist"
)

type BlocklistRouter struct {
	log     *logrus.Logger
	service *blocklist.Service
}

func (router BlocklistRouter) Init() {
	router.service.ListenForNextDNSLogs()
}

func NewBlocklistRouter(service *blocklist.Service) *BlocklistRouter {
	return &BlocklistRouter{log: logging.GetLogger(), service: service}
}

func (router BlocklistRouter) Routes(engine *gin.Engine) {
	var root = fmt.Sprintf("%s/%s", config.GetRootContext(), "block")

	engine.GET(root+"/stats", func(ctx *gin.Context) {
		stats, err := router.service.GetStats(ctx)
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, stats)
		}
	})

	engine.GET(root+"/grouped", func(ctx *gin.Context) {
		servers, err := router.service.GetAllSorted()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.Data(200, "text/plain", []byte(strings.Join(servers, "\n")))
		}
	})

	engine.GET(root+"/list", func(ctx *gin.Context) {
		servers, err := router.service.GetAll()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.Data(200, "text/plain", []byte(strings.Join(servers, "\n")))
		}
	})

	engine.GET(root+"/list.json", func(ctx *gin.Context) {
		servers, err := router.service.GetAll()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, servers)
		}
	})

	engine.GET(root+"/unused", func(ctx *gin.Context) {
		servers, err := router.service.GetUnused()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, servers)
		}
	})

	engine.GET(root+"/used", func(ctx *gin.Context) {
		servers, err := router.service.GetUsed()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, servers)
		}
	})

	engine.GET(root+"/used/latest", func(ctx *gin.Context) {
		servers, err := router.service.GetRecentlyUsed()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, servers)
		}
	})
}

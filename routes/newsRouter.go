package routes

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/go-api/config"
	"gitlab.com/kamackay/go-api/logging"
	"gitlab.com/kamackay/go-api/news"
	"go.mongodb.org/mongo-driver/bson"
	"strconv"
)

type NewsRouter struct {
	log *logrus.Logger
}

func News() *NewsRouter {
	return &NewsRouter{log: logging.GetLogger()}
}

func (router NewsRouter) Init() {
	// Nothing to init
}

func (router NewsRouter) Routes(engine *gin.Engine) {
	var root = config.GetRootContext()

	engine.GET(root+"/news/count", func(ctx *gin.Context) {
		count, err := news.GetCount()
		if err != nil {
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, bson.M{"count": count})
		}
	})

	engine.GET(root+"/news", func(ctx *gin.Context) {
		router.log.Infof("Request on %s", ctx.Request.URL.Path)
		items, err := news.GetAllNews()
		if err != nil {
			router.log.Panic(err)
			ctx.AbortWithStatus(500)
		} else {
			ctx.JSON(200, items)
		}
	})

	engine.GET(root+"/news/after/:time", func(ctx *gin.Context) {
		router.log.Infof("Request on %s", ctx.Request.URL.Path)
		if time, err := strconv.ParseUint(ctx.Param("time"), 10, 0); err != nil {
			ctx.String(400, "Could not parse time into Int")
		} else {
			items, err := news.GetNewsAfter(time)
			if err != nil {
				router.log.Panic(err)
				ctx.AbortWithStatus(500)
			} else {
				ctx.JSON(200, items)
			}
		}
	})

	engine.GET(root+"/news/ids_after/:time", func(ctx *gin.Context) {
		router.log.Infof("Request on %s", ctx.Request.URL.Path)
		if time, err := strconv.ParseUint(ctx.Param("time"), 10, 0); err != nil {
			ctx.String(400, "Could not parse time into Int")
		} else {
			ids, err := news.GetIdsAfter(time)
			if err != nil {
				router.log.Panic(err)
				ctx.AbortWithStatus(500)
			} else {
				ctx.JSON(200, ids)
			}
		}
	})
}

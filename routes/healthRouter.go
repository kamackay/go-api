package routes

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/go-api/logging"
	"gitlab.com/kamackay/go-api/service/health"
)

type HealthRouter struct {
	log     *logrus.Logger
	service *health.Service
}

func Health(service *health.Service) *HealthRouter {
	return &HealthRouter{log: logging.GetLogger(), service: service}
}

func (router HealthRouter) Init() {
	// Nothing to init
}

func (router HealthRouter) Routes(engine *gin.Engine) {

	engine.GET("/ping", func(c *gin.Context) {
		if router.service.IsHealthy() {
			c.JSON(200, gin.H{
				"ok": true,
			})
		} else {
			router.log.Warn("Health check failed")
			c.JSON(500, gin.H{
				"ok": false,
			})
		}
	})
}

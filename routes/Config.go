package routes

import "github.com/gin-gonic/gin"

type Config struct {
	path     string
	function func(context *gin.Context)
}

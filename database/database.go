package database

import (
	"context"
	"gitlab.com/kamackay/go-api/logging"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
)

type DB struct {
	Api *mongo.Database
}

var instantiated *DB = nil
var once sync.Once
var log = logging.GetLogger()

func New() *DB {
	once.Do(func() {
		clientOptions := options.Client().ApplyURI("mongodb://db:27017")
		client, err := mongo.Connect(context.Background(), clientOptions)
		if err != nil {
			log.Error("Could not connect to Mongo")
			panic(err)
		}
		err = client.Ping(context.TODO(), nil)
		if err != nil {
			log.Panic(err)
		}
		instantiated = &DB{}
		instantiated.Api = client.Database("api")
	})
	return instantiated
}

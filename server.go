package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	"gitlab.com/kamackay/go-api/database"
	"gitlab.com/kamackay/go-api/logging"
	"gitlab.com/kamackay/go-api/persist"
	"gitlab.com/kamackay/go-api/routes"
	"gitlab.com/kamackay/go-api/service/blocklist"
	"gitlab.com/kamackay/go-api/service/health"
	// brotli "github.com/Solorad/gin-brotli"
)

func main() {
	log := logging.GetLogger()
	// Instantiate a new router
	engine := gin.Default()
	// engine.Use(brotli.Brotli(brotli.Options{
	// 	Quality: 2, // Default: 4
	// 	LGWin:   11,
	// }))
	engine.Use(gzip.Gzip(gzip.BestCompression))
	engine.Use(cors.Default())
	engine.Use(logger.SetLogger())

	allRoutes := getRoutes()

	for _, router := range allRoutes {
		router.Routes(engine)
	}

	go func() {
		// Run non-route initialization here
		for _, router := range allRoutes {
			router.Init()
		}
	}()

	if err := engine.Run(); err != nil {
		panic(err)
	} else {
		log.Info("Successfully Started Server")
	}
}

func getRoutes() []routes.EndpointGroup {
	db := database.New()
	persistedDataService := persist.NewService(db)

	healthService := health.NewService()

	blocklistService := blocklist.NewService(healthService, db, persistedDataService)
	blocklistService.InitVerifier()

	return append(make([]routes.EndpointGroup, 0),
		routes.News(), routes.NewBlocklistRouter(blocklistService),
		routes.Health(healthService))
}

package utils

import (
	"encoding/json"
	"fmt"
)

// MarshalJSONToMap Function to marshal a JSON string into a map
func MarshalJSONToMap(jsonStr string) (map[string]interface{}, error) {
	var result map[string]interface{}
	err := json.Unmarshal([]byte(jsonStr), &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// PrettyPrintJSON Function to pretty-print a JSON string
func PrettyPrintJSON(data map[string]interface{}) (string, error) {
	// Marshal the map with indentation for pretty printing
	prettyJSONBytes, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return "", err
	}

	// Print the pretty JSON string
	return fmt.Sprintf(string(prettyJSONBytes)), nil
}

func Pointer[T any](o T) *T {
	return &o
}

func LookupOrAdd[T any, K comparable](m map[K]T, key K, valueLookup func() T) T {
	if val, ok := m[key]; ok {
		return val
	}
	val := valueLookup()
	m[key] = val
	return val
}
